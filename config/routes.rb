Rails.application.routes.draw do
  resources :invoice_headers do
    get 'details', on: :member
  end

  resources :invoice_lines do
    get 'details', on: :member
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

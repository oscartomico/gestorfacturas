require 'test_helper'

class InvoiceHeadersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get invoice_headers_index_url
    assert_response :success
  end

  test "should get add" do
    get invoice_headers_add_url
    assert_response :success
  end

  test "should get delete" do
    get invoice_headers_delete_url
    assert_response :success
  end

  test "should get edit" do
    get invoice_headers_edit_url
    assert_response :success
  end

end

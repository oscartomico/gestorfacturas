require 'test_helper'

class InvoiceLinesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get invoice_lines_index_url
    assert_response :success
  end

  test "should get add" do
    get invoice_lines_add_url
    assert_response :success
  end

  test "should get delete" do
    get invoice_lines_delete_url
    assert_response :success
  end

  test "should get edit" do
    get invoice_lines_edit_url
    assert_response :success
  end

end

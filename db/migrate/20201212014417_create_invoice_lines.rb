class CreateInvoiceLines < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_lines do |t|
      t.string :nombre_producto
      t.integer :cantidad
      t.float :precio_unidad
      t.string :color
      t.float :total

      t.timestamps
    end
  end
end

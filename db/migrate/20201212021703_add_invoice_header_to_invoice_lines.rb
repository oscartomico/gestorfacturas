class AddInvoiceHeaderToInvoiceLines < ActiveRecord::Migration[5.1]
  def change
    add_reference :invoice_lines, :invoice_header, foreign_key: true
  end
end

class CreateInvoiceHeaders < ActiveRecord::Migration[5.1]
  def change
    create_table :invoice_headers do |t|
      t.string :nombre_cliente
      t.string :apellidos_cliente
      t.string :dni
      t.string :direccion
      t.string :telefono
      t.string :email
      t.float :tasas
      t.float :total

      t.timestamps
    end
  end
end

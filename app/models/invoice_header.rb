class InvoiceHeader < ApplicationRecord
    has_many :invoice_lines

    
    def destroy_lines
        self.invoice_lines.destroy_all
    end
end

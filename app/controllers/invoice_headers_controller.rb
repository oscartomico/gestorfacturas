class InvoiceHeadersController < ApplicationController

  def index
    @invoice_headers = InvoiceHeader.all
  end

  def new
    @invoice_header = InvoiceHeader.new
  end

  def create
    @invoice_header = InvoiceHeader.new(invoice_header_params)
    if @invoice_header.save
      redirect_to action: :index
    end
  end

  def destroy
    @invoice_header = InvoiceHeader.find_by_id(params[:id])
    @invoice_header.destroy_lines
    @invoice_header.destroy
    redirect_to action: :index, notice: 'Factura eliminada con éxito'
  end

  def edit
    @invoice_header = InvoiceHeader.find_by_id(params[:id])
  end

  def update
    @invoice_header = InvoiceHeader.find_by_id(params[:id])
    if @invoice_header.update(invoice_header_params)
      redirect_to action: :index, notice: 'Linea modificada con éxito'
    end
  end

  def details
    @invoice_header = InvoiceHeader.find_by_id(params[:id])
    @invoice_lines = @invoice_header.invoice_lines.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "details", template: "invoice_headers/details.html.erb"
      end
    end
  end

  private
    def set_invoice_header
      @invoice_header = InvoiceHeader.find(params[:id])
    end

    def invoice_header_params
      params.require(:invoice_header).permit(:id, :nombre_cliente, :apellidos_cliente, :dni, :direccion, :telefono, :email, :tasas, :total)
    end 
end

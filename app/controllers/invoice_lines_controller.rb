class InvoiceLinesController < ApplicationController
  def index
    @invoice_lines = InvoiceLine.all
  end

  def new
    @invoice_line = InvoiceLine.new
    @invoice_headers = InvoiceHeader.all.collect {|p| [ p.nombre_cliente, p.id ] }
  end

  def create
    @invoice_line = InvoiceLine.new(invoice_line_params)
    if @invoice_line.save
      redirect_to action: :index
    end
  end

  def destroy
    @invoice_line = InvoiceLine.find_by_id(params[:id])
    @invoice_line.destroy
    redirect_to action: :index, notice: 'Linea eliminada con éxito'
  end

  def details
    @invoice_line = InvoiceLine.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "details", template: "invoice_lines/details.html.erb"
      end
    end
  end

  def edit
    @invoice_line = InvoiceLine.find_by_id(params[:id])
    @invoice_headers = InvoiceHeader.all.collect {|p| [ p.nombre_cliente, p.id ] }
  end

  def update
    @invoice_line = InvoiceLine.find_by_id(params[:id])
    if @invoice_line.update(invoice_line_params)
      redirect_to action: :index, notice: 'Linea modificada con éxito'
    end
  end

  private
    def set_invoice_line
      @invoice_line = InvoiceLine.find(params[:id])
    end

    def invoice_line_params
      params.require(:invoice_line).permit(:nombre_producto, :cantidad, :precio_unidad, :color, :total, :invoice_header_id)
    end
end
